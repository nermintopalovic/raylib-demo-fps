# raylib-demo-fps

https://www.raylib.com/

# API:

https://www.raylib.com/cheatsheet/cheatsheet.html

https://www.raylib.com/cheatsheet/raymath_cheatsheet.html

# Compile Linux command:

gcc main.c -o program -lraylib -lGL -lm -lpthread -ldl -lrt -lX11

# Working on Windows:

https://github.com/raysan5/raylib/wiki/Working-on-Windows

# Working on Mac:

https://github.com/raysan5/raylib/wiki/Working-on-macOS
