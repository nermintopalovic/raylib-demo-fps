#include "raylib.h"
#include "rlgl.h"
#include "raymath.h"

typedef struct Player {
    Vector3 position;
    Vector3 direction;
    Camera3D camera;
    float height;
    float yaw;
    float pitch;
    float movementSpeed;
    float verticalVelocity;
    bool isGrounded;
    Model weaponModel;
    bool isRecoiling;
    float recoilOffset;
    float recoilSpeed;
} Player;

void InitPlayer (Player *player) {
    player->position = (Vector3) { 0.0f, 0.0f, 0.0f };
    player->direction = (Vector3) { 0.0f, 0.0f, -1.0f }; // facing along negative z axis
    player->height = 2.0f;
    player->yaw = 0.0f;
    player->pitch = 0.0f;
    player->movementSpeed = 15.0f;
    player->verticalVelocity = 0.0f;
    player->isGrounded = true;
    player->isRecoiling = false;
    player->recoilOffset = 0.0f;
    player->recoilSpeed = 0.1f;

    // model loading
    player->weaponModel = LoadModel("assets/scene.gltf");
    player->weaponModel.transform = MatrixMultiply(MatrixRotateY(-90 * DEG2RAD), MatrixRotateZ(-90 * DEG2RAD));
    Texture2D weaponTexture = LoadTexture("assets/textures/Mause_98k_baseColor.png");

    if (player->weaponModel.materialCount > 0) {
        SetMaterialTexture(&player->weaponModel.materials[0], MATERIAL_MAP_DIFFUSE, weaponTexture);
    }

    player->camera.position = (Vector3) { player->position.x, player->position.y + player->height, player->position.z };
    player->camera.target = Vector3Add(player->camera.position, player->direction);
    player->camera.up = (Vector3) { 0.0f, 1.0f, 0.0f };
    player->camera.fovy = 45.0f;
    player->camera.projection = CAMERA_PERSPECTIVE;
}

void DrawPlayerWeaponModel (Player *player) {
    // Calculate weapon position relative to the player
    // Offset the weapon slightly to the right, and in front of the camera
    Vector3 modelOffset = { 0.15f, -0.2f, 0.7f }; // adjust values as needed
    Vector3 forward = Vector3Normalize(Vector3Subtract(player->camera.target, player->camera.position));
    Vector3 right = Vector3CrossProduct(forward, (Vector3){0.0f, 1.0f, 0.0f});
    Vector3 up = Vector3CrossProduct(right, forward);

    // position the weapon by applying the forward and right offsets
    Vector3 weaponPosition = Vector3Add(player->camera.position, Vector3Scale(forward, modelOffset.z));
    weaponPosition = Vector3Add(weaponPosition, Vector3Scale(right, modelOffset.x));
    weaponPosition = Vector3Add(weaponPosition, Vector3Scale(up, modelOffset.y));

    Vector3 weaponRotation = {0.0f, 1.0f, 0.0f};

    DrawModelEx(player->weaponModel, weaponPosition, weaponRotation, player->yaw * RAD2DEG, (Vector3){1.0f, 1.0f, 1.0f}, WHITE);
}

void UpdatePlayerCamera (Player *player) {
    const float mouseSensivity = 0.003f;
    int mouseXDelta = GetMouseDelta().x;
    int mouseYDelta = GetMouseDelta().y;

    player->yaw -= mouseXDelta * mouseSensivity;
    player->pitch -= mouseYDelta * mouseSensivity;

    player->pitch = Clamp(player->pitch, -PI / 2.0f + 1.0f, PI / 2.0f - 1.0f);

    Vector3 direction;
    direction.x = cos(player->pitch) * sin(player->yaw);
    direction.y = sin(player->pitch);
    direction.z = cos(player->pitch) * cos(player->yaw);

    player->camera.target = Vector3Add(player->camera.position, direction);
}

void UpdatePlayerMovement (Player *player, float deltaTime, Sound jumpSound) {
    // gravity handling
    const float gravity = -19.81f; // meters per second squared
    const float jumpVelocity = 8.0f; // initial velocity on jump

    // apply gravity
    if (!player->isGrounded) {
        player->verticalVelocity += gravity * deltaTime;
    }

    // check if player is on ground
    if (player->position.y + player->verticalVelocity * deltaTime < 0) {
        player->position.y = 0;
        player->verticalVelocity = 0;
        player->isGrounded = true;
    } else {
        player->position.y += player->verticalVelocity * deltaTime;
    }

    if (IsKeyPressed(KEY_SPACE) && player->isGrounded) {
        player->verticalVelocity = jumpVelocity;
        player->isGrounded = false;
        PlaySound(jumpSound);
    }

    // movement logic
    Vector3 forward = Vector3Normalize(Vector3Subtract(player->camera.target, player->camera.position));
    Vector3 right = Vector3Normalize(Vector3CrossProduct(forward, (Vector3) { 0.0f, 1.0f, 0.0f }));

    forward.y = 0;
    right.y = 0;

    if (IsKeyDown(KEY_W)) {
        player->position = Vector3Add(player->position, Vector3Scale(forward, player->movementSpeed * deltaTime));
    }
    if (IsKeyDown(KEY_S)) {
        player->position = Vector3Subtract(player->position, Vector3Scale(forward, player->movementSpeed * deltaTime));
    }
    if (IsKeyDown(KEY_A)) {
        player->position = Vector3Subtract(player->position, Vector3Scale(right, player->movementSpeed * deltaTime));
    }
    if (IsKeyDown(KEY_D)) {
        player->position = Vector3Add(player->position, Vector3Scale(right, player->movementSpeed * deltaTime));
    }

    player->camera.position = (Vector3){ player->position.x, player->position.y + player->height, player->position.z };
    player->camera.target = Vector3Add(player->camera.position, Vector3Scale(forward, 1.0f));
}

void UpdateRecoil (Player *player, float deltaTime) {
    if (player->isRecoiling) {
        player->recoilOffset += player->recoilSpeed;
        if (player->recoilOffset > 1.0f) {
            player->isRecoiling = false;
        }
    } else {
        if (player->recoilOffset > 0.0f) {
            player->recoilOffset -= player->recoilSpeed * 0.5f;
        }
    }
    player->recoilOffset = fmax(player->recoilOffset, 0.0f);
}


int main (void) {
    SetConfigFlags(FLAG_WINDOW_RESIZABLE);
    InitWindow(1200, 650, "raylib [core] demo");
    MaximizeWindow();
    SetTargetFPS(60);

    // init audio
    InitAudioDevice();
    Sound gunEffect = LoadSound("assets/shot.mp3");
    Sound jumpEffect = LoadSound("assets/jump.mp3");

    Player player = {0};
    InitPlayer(&player);

    int update (float deltaTime) {
        UpdatePlayerMovement(&player, deltaTime, jumpEffect);
        UpdatePlayerCamera(&player);

        if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
            PlaySound(gunEffect);
            player.isRecoiling = true;
        }

        return 0;
    }

    // setup congrats label
    const char *message = "Congrats Nerko!";
    int fontSize = 20;
    int textWidth = MeasureText(message, fontSize);
    int xPosition = (GetScreenWidth() / 2.0f) - (textWidth / 2.0);

    while (!WindowShouldClose()) {
        float dt = GetFrameTime();

        // update
        update(dt);

        DisableCursor();
        SetMousePosition(GetScreenWidth() / 2.0f, GetScreenHeight() / 2.0f);

        BeginDrawing();
            ClearBackground(RAYWHITE);

            BeginMode3D(player.camera);
                DrawGrid(100, 10.0f);
                DrawPlayerWeaponModel(&player);
            EndMode3D();

            DrawText(message, xPosition, 200, 20, LIGHTGRAY);
            DrawFPS(10, 1.0f);
        EndDrawing();
    }

    UnloadSound(gunEffect);
    UnloadSound(jumpEffect);
    CloseAudioDevice();
    CloseWindow();
    return 0;
}
